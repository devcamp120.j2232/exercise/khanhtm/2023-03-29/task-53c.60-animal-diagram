import s50.*;
public class Task53C60 {
    public static void main(String[] args) throws Exception {
        Cat cat = new Cat("Kitty");
        Dog dog = new Dog("Becky");
        BigDog bigdog = new BigDog("Lucifer");
        cat.greets();
        dog.greets();
        bigdog.greets();
        bigdog.greets(new BigDog("Golden"));

    }
}
